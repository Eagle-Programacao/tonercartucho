<?php
	get_header( 'shop' );
	global $product;
?>

<script type="text/javascript" language="javascript" src="<?php echo get_bloginfo('template_url'); ?>/_assets/js/relatorio/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo get_bloginfo('template_url'); ?>/_assets/js/relatorio/dataTables.buttons.min.js">
</script>
<script type="text/javascript" language="javascript" src="<?php echo get_bloginfo('template_url'); ?>/_assets/js/relatorio/buttons.flash.min.js">
</script>
<script type="text/javascript" language="javascript" src="<?php echo get_bloginfo('template_url'); ?>/_assets/js/relatorio/jszip.min.js">
</script>
<script type="text/javascript" language="javascript" src="<?php echo get_bloginfo('template_url'); ?>/_assets/js/relatorio/pdfmake.min.js">
</script>
<script type="text/javascript" language="javascript" src="<?php echo get_bloginfo('template_url'); ?>/_assets/js/relatorio/vfs_fonts.js">
</script>
<script type="text/javascript" language="javascript" src="<?php echo get_bloginfo('template_url'); ?>/_assets/js/relatorio/buttons.html5.min.js">
</script>
<script type="text/javascript" language="javascript" src="<?php echo get_bloginfo('template_url'); ?>/_assets/js/relatorio/buttons.print.min.js">
</script>


<link rel='stylesheet' id='contact-form-7-css'  href='<?php echo get_bloginfo('template_url'); ?>/_assets/css/relatorio/jquery.dataTables.min.css' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='<?php echo get_bloginfo('template_url'); ?>/_assets/css/relatorio/buttons.dataTables.min.css' type='text/css' media='all' />
<script type="text/javascript">
jQuery(document).ready(function($) {
	$('#v_admin').DataTable( {
		dom: 'Bfrtip',
		buttons: [
			{
				extend: 'copyHtml5',
				exportOptions: {
				 columns: ':contains("Office")'
				}
			},
			'excelHtml5'
		]
	} );
	$('#v_geovanna').DataTable( {
		dom: 'Bfrtip',
		buttons: [
			{
				extend: 'copyHtml5',
				exportOptions: {
				 columns: ':contains("Office")'
				}
			},
			'excelHtml5'
		]
	} );
	$('#v_kamyla').DataTable( {
		dom: 'Bfrtip',
		buttons: [
			{
				extend: 'copyHtml5',
				exportOptions: {
				 columns: ':contains("Office")'
				}
			},
			'excelHtml5'
		]
	} );	
	jQuery(".buttons-html5:nth-child(1) span").text("Copiar");
} );	
</script>

<?php
	/*
	$author_obj = get_user_by('id', 4);
	echo $author_obj -> display_name;
	*/
?>

<section class="relatorio">
	<div class="container">
		<div class="row">
			<ul class="nav nav-tabs" role="tablist">
				<li class="nav-item active">
					<a class="nav-link active" data-toggle="tab" href="#admin" role="tab">Admin</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" data-toggle="tab" href="#julio" role="tab">julio</a>
				</li>				
				<li class="nav-item">
					<a class="nav-link" data-toggle="tab" href="#geovanna" role="tab">Geovanna</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" data-toggle="tab" href="#kamyla" role="tab">Kamyla</a>
				</li>				
			</ul>
		</div>
		<div class="row">
			<div class="tab-content">
			  <div class="tab-pane active" id="admin" role="tabpanel">
					<table id="v_admin" class="display dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
						<thead>
							<tr role="row">
								<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 97px;">
									Produto
								</th>
								<th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 158px;">
								Descrição do Produto
								</th>
								<th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 72px;">
								Produtos Compatíveis
								</th>
								<th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 27px;">
								Preço
								</th>
								<th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 63px;">
								Data
								</th>
								<th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 56px;">
								Características Técnicas
								</th>
								<th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 56px;">
								Indicações de Uso
								</th>							
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th rowspan="1" colspan="1">
									Produto
								</th>
								<th rowspan="1" colspan="1">
									Descrição do Produto
								</th>
								<th rowspan="1" colspan="1">
									Produtos Compatíveis
								</th>
								<th rowspan="1" colspan="1">
									Preço
								</th>
								<th rowspan="1" colspan="1">
									Data
								</th>
								<th rowspan="1" colspan="1">
									Características Técnicas
								</th>
								<th rowspan="1" colspan="1">
									Indicações de Uso
								</th>							
							</tr>
						</tfoot>
						<tbody>
							<?php
								$r = 0;
								$julio = array(
									'posts_per_page' => '10000',
									'post_type' => 'product',
									'author' => 1 									
								);

								$wc_query = new WP_Query($julio);
								if ($wc_query->have_posts()):
									while ($wc_query->have_posts()):
										$wc_query->the_post();    
							?>
								<tr role="row" class="<?php if($r % 2 == 0){echo "odd"; }else{echo "even";} ?>">
										<td class="sorting_1"><?php the_title(); ?></td>
										<td>
										  	<?php					  		
										  		if(get_field("descrição_detalhada_do_produto", get_the_ID()) != null)
										  		{
										  			echo get_field("descrição_detalhada_do_produto", get_the_ID());
										  		}else
										  		{
										  			echo "Descrição indisponível para este produto.";
										  		}					  		
										  	?>											
										</td>
										<td>
										  	<?php
										  		if(get_field("produtos_compativeis", get_the_ID()) != null)
										  		{
										  			echo get_field("produtos_compativeis", get_the_ID());
										  		}else
										  		{
										  			echo "Sem Produtos compatíveis disponíveis para este produto.";
										  		}
										  	?>											
										</td>
										<td><?php echo $product->get_price_html(); ?></td>
										<td><?php echo get_the_date(); ?></td>
										<td>
										  	<?php
										  		if(get_field("caracteristicas_tecnicas", get_the_ID()) != null)
										  		{
										  			echo get_field("caracteristicas_tecnicas", get_the_ID());
										  		}else
										  		{
										  			echo "Características Técnicas indisponíveis para este produto.";
										  		}						  						  		
										  	?>											
										</td>
										<td>
										  	<?php
										  		if(get_field("indicacoes_de_uso", get_the_ID()) != null)
										  		{
										  			echo get_field("indicacoes_de_uso", get_the_ID());
										  		}else
										  		{
										  			echo "Indicações de Uso indisponíveis para este produto.";
										  		}
										  	?>										
										</td>
								</tr>
									<?php
										$r++;
										endwhile;
									?>
								<?php wp_reset_postdata(); wp_reset_query(); ?>
							<?php endif; ?>  
						</tbody>
					</table>  	
			  </div>
			  <div class="tab-pane" id="julio" role="tabpanel">
					<table id="v_geovanna" class="display dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
						<thead>
							<tr role="row">
								<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 97px;">
									Produto
								</th>
								<th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 158px;">
								Descrição do Produto
								</th>
								<th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 72px;">
								Produtos Compatíveis
								</th>
								<th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 27px;">
								Preço
								</th>
								<th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 63px;">
								Data
								</th>
								<th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 56px;">
								Características Técnicas
								</th>
								<th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 56px;">
								Indicações de Uso
								</th>							
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th rowspan="1" colspan="1">
									Produto
								</th>
								<th rowspan="1" colspan="1">
									Descrição do Produto
								</th>
								<th rowspan="1" colspan="1">
									Produtos Compatíveis
								</th>
								<th rowspan="1" colspan="1">
									Preço
								</th>
								<th rowspan="1" colspan="1">
									Data
								</th>
								<th rowspan="1" colspan="1">
									Características Técnicas
								</th>
								<th rowspan="1" colspan="1">
									Indicações de Uso
								</th>							
							</tr>
						</tfoot>
						<tbody>
							<?php
								$r = 0;
								$geovanna = array(
									'posts_per_page' => '10000',
									'post_type' => 'product',
									'author' => 2 									
								);

								$wc_query = new WP_Query($geovanna);
								if ($wc_query->have_posts()):
									while ($wc_query->have_posts()):
										$wc_query->the_post();    
							?>
								<tr role="row" class="<?php if($r % 2 == 0){echo "odd"; }else{echo "even";} ?>">
										<td class="sorting_1"><?php the_title(); ?></td>
										<td>
										  	<?php					  		
										  		if(get_field("descrição_detalhada_do_produto", get_the_ID()) != null)
										  		{
										  			echo get_field("descrição_detalhada_do_produto", get_the_ID());
										  		}else
										  		{
										  			echo "Descrição indisponível para este produto.";
										  		}					  		
										  	?>											
										</td>
										<td>
										  	<?php
										  		if(get_field("produtos_compativeis", get_the_ID()) != null)
										  		{
										  			echo get_field("produtos_compativeis", get_the_ID());
										  		}else
										  		{
										  			echo "Sem Produtos compatíveis disponíveis para este produto.";
										  		}
										  	?>											
										</td>
										<td><?php echo $product->get_price_html(); ?></td>
										<td><?php echo get_the_date(); ?></td>
										<td>
										  	<?php
										  		if(get_field("caracteristicas_tecnicas", get_the_ID()) != null)
										  		{
										  			echo get_field("caracteristicas_tecnicas", get_the_ID());
										  		}else
										  		{
										  			echo "Características Técnicas indisponíveis para este produto.";
										  		}						  						  		
										  	?>											
										</td>
										<td>
										  	<?php
										  		if(get_field("indicacoes_de_uso", get_the_ID()) != null)
										  		{
										  			echo get_field("indicacoes_de_uso", get_the_ID());
										  		}else
										  		{
										  			echo "Indicações de Uso indisponíveis para este produto.";
										  		}
										  	?>										
										</td>
								</tr>
									<?php
										$r++;
										endwhile;
									?>
								<?php wp_reset_postdata(); wp_reset_query(); ?>
							<?php endif; ?>  
						</tbody>
					</table>  	
			  </div>			  
			  <div class="tab-pane" id="geovanna" role="tabpanel">
					<table id="v_geovanna" class="display dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
						<thead>
							<tr role="row">
								<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 97px;">
									Produto
								</th>
								<th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 158px;">
								Descrição do Produto
								</th>
								<th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 72px;">
								Produtos Compatíveis
								</th>
								<th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 27px;">
								Preço
								</th>
								<th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 63px;">
								Data
								</th>
								<th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 56px;">
								Características Técnicas
								</th>
								<th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 56px;">
								Indicações de Uso
								</th>							
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th rowspan="1" colspan="1">
									Produto
								</th>
								<th rowspan="1" colspan="1">
									Descrição do Produto
								</th>
								<th rowspan="1" colspan="1">
									Produtos Compatíveis
								</th>
								<th rowspan="1" colspan="1">
									Preço
								</th>
								<th rowspan="1" colspan="1">
									Data
								</th>
								<th rowspan="1" colspan="1">
									Características Técnicas
								</th>
								<th rowspan="1" colspan="1">
									Indicações de Uso
								</th>							
							</tr>
						</tfoot>
						<tbody>
							<?php
								$r = 0;
								$geovanna = array(
									'posts_per_page' => '10000',
									'post_type' => 'product',
									'author' => 2 									
								);

								$wc_query = new WP_Query($geovanna);
								if ($wc_query->have_posts()):
									while ($wc_query->have_posts()):
										$wc_query->the_post();    
							?>
								<tr role="row" class="<?php if($r % 2 == 0){echo "odd"; }else{echo "even";} ?>">
										<td class="sorting_1"><?php the_title(); ?></td>
										<td>
										  	<?php					  		
										  		if(get_field("descrição_detalhada_do_produto", get_the_ID()) != null)
										  		{
										  			echo get_field("descrição_detalhada_do_produto", get_the_ID());
										  		}else
										  		{
										  			echo "Descrição indisponível para este produto.";
										  		}					  		
										  	?>											
										</td>
										<td>
										  	<?php
										  		if(get_field("produtos_compativeis", get_the_ID()) != null)
										  		{
										  			echo get_field("produtos_compativeis", get_the_ID());
										  		}else
										  		{
										  			echo "Sem Produtos compatíveis disponíveis para este produto.";
										  		}
										  	?>											
										</td>
										<td><?php echo $product->get_price_html(); ?></td>
										<td><?php echo get_the_date(); ?></td>
										<td>
										  	<?php
										  		if(get_field("caracteristicas_tecnicas", get_the_ID()) != null)
										  		{
										  			echo get_field("caracteristicas_tecnicas", get_the_ID());
										  		}else
										  		{
										  			echo "Características Técnicas indisponíveis para este produto.";
										  		}						  						  		
										  	?>											
										</td>
										<td>
										  	<?php
										  		if(get_field("indicacoes_de_uso", get_the_ID()) != null)
										  		{
										  			echo get_field("indicacoes_de_uso", get_the_ID());
										  		}else
										  		{
										  			echo "Indicações de Uso indisponíveis para este produto.";
										  		}
										  	?>										
										</td>
								</tr>
									<?php
										$r++;
										endwhile;
									?>
								<?php wp_reset_postdata(); wp_reset_query(); ?>
							<?php endif; ?>  
						</tbody>
					</table>  	
			  </div>
			  <div class="tab-pane" id="kamyla" role="tabpanel">
					<table id="v_kamyla" class="display dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
						<thead>
							<tr role="row">
								<th class="sorting_asc" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 97px;">
									Produto
								</th>
								<th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 158px;">
								Descrição do Produto
								</th>
								<th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 72px;">
								Produtos Compatíveis
								</th>
								<th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 27px;">
								Preço
								</th>
								<th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 63px;">
								Data
								</th>
								<th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 56px;">
								Características Técnicas
								</th>
								<th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 56px;">
								Indicações de Uso
								</th>							
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th rowspan="1" colspan="1">
									Produto
								</th>
								<th rowspan="1" colspan="1">
									Descrição do Produto
								</th>
								<th rowspan="1" colspan="1">
									Produtos Compatíveis
								</th>
								<th rowspan="1" colspan="1">
									Preço
								</th>
								<th rowspan="1" colspan="1">
									Data
								</th>
								<th rowspan="1" colspan="1">
									Características Técnicas
								</th>
								<th rowspan="1" colspan="1">
									Indicações de Uso
								</th>							
							</tr>
						</tfoot>
						<tbody>
							<?php
								$r = 0;
								$kamyla = array(
									'posts_per_page' => '10000',
									'post_type' => 'product',
									'author' => 3 									
								);

								$wc_query = new WP_Query($kamyla);
								if ($wc_query->have_posts()):
									while ($wc_query->have_posts()):
										$wc_query->the_post();    
							?>
								<tr role="row" class="<?php if($r % 2 == 0){echo "odd"; }else{echo "even";} ?>">
										<td class="sorting_1"><?php the_title(); ?></td>
										<td>
										  	<?php					  		
										  		if(get_field("descrição_detalhada_do_produto", get_the_ID()) != null)
										  		{
										  			echo get_field("descrição_detalhada_do_produto", get_the_ID());
										  		}else
										  		{
										  			echo "Descrição indisponível para este produto.";
										  		}					  		
										  	?>											
										</td>
										<td>
										  	<?php
										  		if(get_field("produtos_compativeis", get_the_ID()) != null)
										  		{
										  			echo get_field("produtos_compativeis", get_the_ID());
										  		}else
										  		{
										  			echo "Sem Produtos compatíveis disponíveis para este produto.";
										  		}
										  	?>											
										</td>
										<td><?php echo $product->get_price_html(); ?></td>
										<td><?php echo get_the_date(); ?></td>
										<td>
										  	<?php
										  		if(get_field("caracteristicas_tecnicas", get_the_ID()) != null)
										  		{
										  			echo get_field("caracteristicas_tecnicas", get_the_ID());
										  		}else
										  		{
										  			echo "Características Técnicas indisponíveis para este produto.";
										  		}						  						  		
										  	?>											
										</td>
										<td>
										  	<?php
										  		if(get_field("indicacoes_de_uso", get_the_ID()) != null)
										  		{
										  			echo get_field("indicacoes_de_uso", get_the_ID());
										  		}else
										  		{
										  			echo "Indicações de Uso indisponíveis para este produto.";
										  		}
										  	?>										
										</td>
								</tr>
									<?php
										$r++;
										endwhile;
									?>
								<?php wp_reset_postdata(); wp_reset_query(); ?>
							<?php endif; ?>  
						</tbody>
					</table>  	
			  </div>			  
			</div>	
		</div>
	</div>
</section>
<?php get_footer(); ?>