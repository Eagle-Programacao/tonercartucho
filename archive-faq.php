<?php get_header(); ?>
<div class="faq">
	<div id="nav-bar">
		<p><a href="<?php echo site_url(); ?>"><i class="fa fa-home" aria-hidden="true"></i></a> &gt; <span>Perguntas Frequentes</span></p>
	</div>

	<div class="container hidden-xs">
		<div class="perguntas-frequentes">
			<h2><?php _e("Perguntas Frequentes", "vuelo"); ?></h2>
			<?php
                $terms = get_terms('taxonomy_faq');
            ?>
			<div class="row">
				<div class="col-sm-5 col-categorias">
				  	<ul class="nav nav-tabs" role="tablist">
				  		<?php 
				  		$count = 1;
				  		foreach ( $terms as $term ) { 
				  			?>
					        <li role="presentation" class="<?php if($count === 1){ echo 'active'; } ?>"><a href="#faq-<?php echo $term->slug; ?>" class="term-text" aria-controls="faq-<?php echo $term->slug; ?>" role="tab" data-toggle="tab"><?php echo $term->name; ?></a></li>
					    <?php 
					    $count++;
						} 
						?>
				  	</ul>
				</div>
				<div class="col-sm-7">
					<div class="tab-content">
						<?php 
						$count = 1; 
						foreach ( $terms as $key=>$term ) { ?>
					    <div role="tabpanel" class="tab-pane <?php if($count === 1){ echo 'active'; } ?>" id="faq-<?php echo $term->slug; ?>">
							<div class="panel-group" id="accordion-<?php echo $key; ?>" role="tablist" aria-multiselectable="true">
							<?php 
								$args = array(
			                        'post_type' => 'faq',
			                        'posts_per_page' => 1000,
			                        'tax_query' => array(
										array(
											'taxonomy' => 'taxonomy_faq',
											'field'    => 'slug',
											'terms'    => $term->slug
										)
									)
			                    );
				                query_posts($args);
				                if(have_posts()): while(have_posts()): the_post();
							?>
							  <div class="panel panel-default">
							    <div class="panel-heading" role="tab" id="heading-<?php echo $post->ID; ?>">
							      <h4 class="panel-title">
							        <a role="button" data-toggle="collapse" data-parent="#accordion-<?php echo $key; ?>" href="#collapse-<?php echo $post->ID; ?>" aria-expanded="false" aria-controls="collapse-<?php echo $post->ID; ?>">
							          <?php the_title(); ?>
							        </a>
							      </h4>
							    </div>
							    <div id="collapse-<?php echo $post->ID; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-<?php echo $post->ID; ?>">
							      <div class="panel-body">
							        <?php the_content(); ?>
							      </div>
							    </div>
							  </div>
							  <?php endwhile; endif; ?>
							</div><!-- /panel-group -->
					    </div><!-- /tab-panel -->
					    <?php 
					    $count++; 
						} 
						?>
					</div><!-- /tab-content -->

				</div><!-- /col -->
			</div><!-- /row -->
		</div><!-- /perguntas-frequentes -->
	</div><!-- /container -->

	<div class="container container-faq-mobile hidden-sm hidden-md hidden-lg">
		<?php get_template_part("inc/faq", "mobile"); ?>
	</div>

	<div class="ainda-confuso">
		<div class="container">
			<div class="row">
				<img class="img-interrogacao" src="<?php echo get_template_directory_uri().'/_assets/img/interrogacao.png'; ?>">
				<h5><?php _e("Ainda confuso?", "vuelo"); ?></h5>
				<h5><?php _e("Não encontrou o que procurava?", "vuelo"); ?></h5>
				<div class="btn-fale-conosco-faq">
					<a href="<?php echo site_url(); ?>/contato" class="hvr-wobble-horizontal"><?php _e("Fale Conosco", "vuelo"); ?></a>
				</div>
			</div>
		</div>
	</div>

</div>
<?php get_footer(); ?>