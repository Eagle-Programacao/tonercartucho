<?php get_header( 'shop' ); ?>

<?php
/*
Template Name: Central de Atendimento
*/
?>
<section class="breadcrumb_loja">
	<div class="container">
		<?php
			do_action( 'woocommerce_before_main_content' );
		?>		
	</div>
</section>
<section class="auto_atendimento">
	<div class="container">
		<span>
			<h1>Auto Atendimento 24h</h1>
			<p>Tudo o que você precisa num só lugar. Como podemos ajudar?</p>		
		</span>
		<div class="row atendimento">
			<div class="col-lg-4">
				<h3>Meu Pedido</h3>
				<p>
					<a href="<?php echo get_bloginfo('url');?>/minha-conta/edit-address/">Alterar endereço de entrega</a>
				</p>
				<p>
					<a class="open_modal_whats">Preciso de ajuda para comprar</a>
				</p>
			</div>
			<div class="col-lg-4">
				<h3>Trocas e assistência</h3>
				<p>
					<a class="open_modal_whats">Solicitar Assistência</a>
				</p>					
				<p>
					<a class="open_modal_whats">Solicitar Devolução</a>
				</p>		
			</div>
			<div class="col-lg-4">
				<h3>Cadastro e Conta</h3>
				<p>
					<a href="<?php echo get_bloginfo('url');?>/minha-conta/lost-password/">Recuperar senha</a>
				</p>					
				<p>
					<a href="<?php echo get_bloginfo('url');?>/minha-conta/edit-account/">Alterar email de cadastro</a>
				</p>			
			</div>						
		</div>
		<div class="row navegacao">
			<div class="col-lg-3">
				<ul class="nav nav-tabs nav-stacked" role="tablist">
					<p><b>Meus Pedidos</b></p>				
					<li class="nav-item active">
						<a class="nav-link active" data-toggle="tab" href="#entrega" role="tab">Entrega</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#troca" role="tab">Troca e Devolução</a>
					</li>
					<br>
					<p><b>Antes de Comprar</b></p>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#seguranca" role="tab">Compra segura</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#pagamento" role="tab">Formas de Pagamento</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#aprovacao" role="tab">Pedidos em Aprovação</a>
					</li>
					<br>
					<p><b>Privacidade e Segurança</b></p>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#privacidade" role="tab">Política de Privacidade</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#segurancap" role="tab">Segurança</a>
					</li>				
				</ul>		
			</div>
			<div class="col-lg-9">
				<div class="tab-content">
				  <div class="tab-pane active" id="entrega" role="tabpanel">
					<h5><b>O que significa "Controle de Qualidade" em "Aguardando Produção"?</b></h5>

					<p>Depois que a fábrica recebe seu pedido, ele passa por várias etapas até se transformar no seu móvel. E aqui, você fica por dentro de tudo! </p>
					<p>Numa linha de produção, os móveis são fabricados em lotes de peças para otimizar os recursos naturais e passam por diversos processos. </p>
					<p>Este é o momento de verificar cada se cada detalhe saiu como o planejado!</p>
					<br>

					<h5><b>Porque não tenho um código de rastreio do meu pedido?</b></h5>

					<p>A Tonner & Cartucho entrega realiza as entregas para a maior parte dos clientes via motoboy. Neste caso, diferente do que é utilizado pelos Correios ou transportadoras expressas, a sua encomenda não tem um código de rastreio de entrega.</p>
					<br>
					<h5><b>Posso agendar a entrega? / Como saber o horário da entrega?</b></h5>

					<p>Infelizmente não podemos garantir o agendamento da sua entrega.
					Mas não se preocupe, caso o motoboy/correio não encontre o responsável para receber seu pedido, entraremos em contato para realizar a segunda tentativa.
					Vale lembrar que as entregas são realizadas em dias úteis, de Segunda a Sexta-Feira das 8h às 12h ou das 13h às 18h ;)</p>	
					<br>
					<h5><b>Ainda não recebi meu pedido, o que faço?</b></h5>

					<p>Estamos atentos aos prazos de entrega e temos um processo de acompanhamento pedido a pedido.
					Portanto, é possível que já tenhamos entrado em contato com você por e-mail para informar o novo prazo de entrega. 
					Por favor, verifique sua Caixa de Entrada e SPAM.</p>
					<p>Caso não encontre nosso comunicado, <a class="open_modal_whats">clique aqui</a> para nos enviar um alerta. Seu atendimento será automaticamente priorizado.</p>										

				  </div>

				  <div class="tab-pane" id="troca" role="tabpanel">
					<h5><b>Meu produto veio diferente do anunciado, o que fazer?</b></h5>

					<p>Fazemos de tudo para que sua compra seja tranquila e perfeita! 
					Porém, as cores das fotos podem variar de acordo com o seu monitor e as características reais do produto nem sempre podem ser percebidas perfeitamente através de fotos. Por isso, sempre aplicamos avisos e alertas sobre as imagens do produto em nossas páginas:</p>
					<p>- Além da cor, pequenos detalhes e acabamentos podem não ficar evidentes na foto, por isso as imagens são meramente ilustrativas.
					- Os objetos decorativos que aparecem na foto não acompanham o produto.</p>

					<p>Atenção!
					Se você recebeu o produto diferente do anunciado, não faça a montagem. 
					Tire foto da etiqueta da embalagem e da amostra do acabamento que diverge do produto comprado e nos envie através do formulário de troca.</p>
			

					<p><a class="open_modal_whats">Clique aqui</a> e solicite seu atendimento.</p>
					<br>
					<h5><b>Comprei dois produtos iguais e quero apenas uma unidade, posso?</b></h5>					

					<p>Sim, é possível o cancelamento parcial de seu pedido.
					Entre em contato conosco através do chat.</p> 
					<p><a class="open_modal_whats">Clique aqui aqui</a></p>
				  </div>

				  <div class="tab-pane" id="seguranca" role="tabpanel">
					<h5><b>Entrei no reclame aqui e estou inseguro. Posso comprar mesmo?</b></h5>

					<p>Ahhhh, mas você procurou informações em um site onde só existe reclamações!</p>
					<p>Os atendimentos do Reclame Aqui representam bem menos que 1% dos pedidos realizados na loja. Além disso, todas as reclamações são respondidas e tratadas da melhor forma possível e com muito respeito pelo cliente. Afinal, queremos atender bem para atender sempre! </p>

				  </div>

				  <div class="tab-pane" id="pagamento" role="tabpanel">

					<h5><b>Como funciona o boleto?</b></h5>

					<p>O Boleto Bancário é exclusivo para pagamento à vista! É gerado somente via Pag Seguro garantindo uma compra sem complicação ou burocracia. Pague em qualquer agência bancária, lotérica ou pelo bankline! Se quiser agilizar sua aprovação, realize o pagamento antes das 15h ;)</p>
					<br>
					<b><h5>Quais são as bandeiras de cartão de crédito aceitas?</b></h5>

					<p>Quase todas ;) </p>
					<p>Você pode usar cartões Visa, Mastercard ou Hipercard para decorar do seu jeitinho. 
					Lembre-se que o seu cartão possui um limite de crédito que contabiliza o valor total da compra. Caso necessário, solicite antecipadamente a liberação de crédito com o banco e evite o cancelamento da sua compra.</p>

				  </div>

				  <div class="tab-pane" id="aprovacao" role="tabpanel">

					<h5><b>Porque a minha compra não foi aprovada se o débito consta na fatura do meu cartão?</b></h5>

					<p>Logo depois de finalizar seu pedido, você recebe uma confirmação de sucesso.</p>
					<br>
					<p>Neste momento, alguns bancos já informam o débito. No entanto, a confirmação de pagamento pela operadora é apenas uma das etapas de análise do seu pedido, que passa por processo interno de aprovação. m breve, você receberá um comunicado com os detalhes. </p>
					<p>Fique atendo à sua caixa de e-mails ;)</p>
					<br>
					<h5><b>Quanto tempo demora a aprovação do pedido pago por boleto?</b></h5>
					<p>Mas o seu pedido pode demorar até 04 (quatro) dias úteis para ser aprovado em caso de pagamento com cheque.
					Seu pagamento é automaticamente identificado pelo número do pedido que está dentro do código de barras do boleto, então não é necessário enviar o comprovante de pagamento! </p>
					<br>
					<h5><b>Quanto tempo demora para aprovar um pedido em cartão de crédito?</b></h5>
					<p>O prazo para a liberação do seu pagamento é de no máximo 1 (um) dia útil, mas o seu pedido passa por um procedimento interno que pode demorar até 2 dias úteis.</p>
					<br>
					<h5><b>Fiz o pagamento por depósito. Porque meu pedido ainda não foi aprovado???</b></h5>
					<p>O valor do depósito ou da transferência precisa ser igual o do pedido, pois recebemos um extrato de pagamento com valores recebidos.</p>

					<p>Se você não identificou o pedido, podemos demorar um pouco mais do que 2 dias úteis para confirmar a transação.</p>

					<p>Você pode nos enviar o comprovante da operação??</p>						

				  </div>
				  <div class="tab-pane" id="privacidade" role="tabpanel">

				    <p>Sabemos que você preza muito pela sua segurança e privacidade, por isso usamos os melhores softwares do mercado e firmamos o compromisso de não divulgar seus dados cadastrais em nenhuma hipótese para que sua experiência de compra seja excelente do início ao fim. As informações que você nos confia são utilizadas, exclusivamente, para facilitar a prestação de serviços e personalizar o seu atendimento</p>

					<p>Veja abaixo alguns dos nossos procedimentos:</p>
					<br>					
					<p>Sabemos que você preza muito pela sua privacidade, por isso estabelecemos uma série de regras para garantir que os seus dados cadastrais estejam seguros. As informações que você nos confia são utilizadas, exclusivamente, para facilitar a prestação de serviços e personalizar o seu atendimento.</p>
					<br>
					<h5><b>Coleta e uso das informações:</b></h5>
					<p>Para seu cadastro, solicitamos informações como nome, endereço, e-mail, telefones para contato e etc. Os seus dados são necessários para facilitar suas compras no site e garantir que seu produto chegue com assertividade e segurança no endereço de entrega.</p>
					<br>
					<h5><b>Envio de e-mails:</b></h5>
					<p>Nunca enviamos e-mails solicitando confirmação de dados/cadastro ou com anexos executáveis, como por exemplo: extensão .exe, .com, .scr, .bat, dentre outros e links para download.</p>
					<p>Se você usar ou tiver instalando algum serviço de Anti Spam, você precisa autorizar o recebimento das mensagens vindas do domínio tonercartucho.com.br. Só assim poderemos manter contato com você. Os links de nossos e-mails levam diretamente para a tonercartucho.com.br.</p>							
				  </div>
				  <div class="tab-pane" id="segurancap" role="tabpanel">
					<h5><b>Como identificar a segurança?</b></h5>
					<p>A melhor forma de identificar a segurança é através do seu navegador (browser), ele indica se você está em uma página HTTPS totalmente segura. Disponibilizar um site seguro é o mínimo que podemos oferecer para você :)</p>
					<p>Lembre-se: mesmo pela internet, o cliente está sempre amparado pelo Código de Defesa do Consumidor, você pode consultar a seriedade da loja em diversos sites e ferramentas que fiscalizam os comércios eletrônicos e, por fim, os bancos são obrigados a restituir qualquer valor retirado indevidamente da sua conta bancária.</p>
					<br>
					<p>O Google faz um relatório de segurança de todos os sites, <a href="https://www.google.com/transparencyreport/safebrowsing/diagnostic/?hl=pt-BR#url=https://tonercartucho.com.br/" target="_blank">Clique Aqui</a> e verifique a nossa página para se sentir mais seguro.</p>							
				  </div>				  
				</div>
			</div>	
		</div>	
	</div>
</section>

<?php get_footer(); ?>