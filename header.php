<!DOCTYPE html>
<html>
<head>
	<title><?php echo wp_title('|', true, 'right') . get_bloginfo('name'); ?></title>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, user-scalable=no">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_bloginfo('template_url')?>/_assets/img/favicon2.ico">
	
	<title><?php echo get_bloginfo("name") . wp_title(" | "); ?></title>

	<script>var theme_directory = "<?php echo get_template_directory_uri() ?>";</script>

	<!--[if lt IE 9]>
	<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">

	<?php wp_head(); ?>

	<?php
		if(is_cart()){
	?>
		<script type="text/javascript">
			jQuery(".shipping-calculator-button").trigger("click");
		</script>
	<?php
		}
	?>
	<div>
	<script> 
		var $buoop = {vs:{i:8,f:-8,o:-8,s:7,c:-8},api:4}; 
		function $buo_f(){ 
		 var e = document.createElement("script"); 
		 e.src = "//browser-update.org/update.min.js"; 
		 document.body.appendChild(e);
		};
		try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
		catch(e){window.attachEvent("onload", $buo_f)}
	</script>

	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/_assets/css/animate.css">
</div>
</head>

<body class="<?php if(is_front_page()){ echo 'home'; } else if(is_product()){ echo 'internas single-produto menu_relative'; } else { echo 'internas menu_relative'; } ?>" <?php body_class(); ?>>
	<div id="fb-root"></div>
	<div class="identify-home-js" style='visibility: hidden; position: absolute;'><?php if(is_home()) { echo "homejs"; } ?></div>
	<div id="all">
		<div class="cont" class="container">
			<header>
				<div class="menu_header">
					<div class="div-border"></div>
					<?php include 'produtos-header.php'; ?>
					<div class="menu_superior">
						<div class="container">
							<div class="row">
								<div class="col-sm-3">
									<div class="opcoes_superior">						
										<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>">
											<span>
												Faça login ou cadastre - se
											</span>
										</a>										
									</div>								
								</div>
								<div class="col-sm-6">
									<div class="col-lg-5 col-md-5">
									</div>
									<div class="col-lg-3 col-md-4">
									<!-- 	<i class="fa fa-whatsapp" aria-hidden="true"></i>
										<p>41 | 9 0000 - 0000</p> -->
									</div>
									<div class="col-lg-4 col-md-3">									
									</div>
								</div>									
								<div class="segundo_whats">
									<i class="fa fa-whatsapp" aria-hidden="true"></i>
									<p>41 | 9 0000 - 0000</p>								
								</div>
								<!-- <div class="col-sm-3 boleto"><p><a href="">2° Via boleto</a></p></div> -->
							</div>
						</div>
					</div>

					<div class="menu_normal">
						<div id="menu_inferior">
							<div class="container">
								<div class="row">
									<div class="col-sm-2 logo_menu">
										<div class="menu_mobile_div">
											<button class="hamburger">&#9776;</button>
											<button class="cross">&#735;</button>
										</div>

										<div class="logo">
											<div class="align_logo">
												<a href="<?php echo get_bloginfo('url'); ?>">
													<img src="<?php echo get_bloginfo('template_url').'/_assets/img/logo-branca.png'; ?>">
												</a>	
											</div>
										</div>							
									</div>
									<div class="col-sm-8 busca">
										<div class="align_opcoes">
											<div class="o coes_inferior">
												<form class="search-box" action="<?php bloginfo('url'); ?>" method="GET">
													<input type="text" name="s" class="form-control" placeholder="<?php _e("O que você procura?", "vuelo"); ?>">
													<button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
												</form>
											</div>				
										</div>								
									</div>
									<div class="col-sm-2 icons_ecommerce">
										<div class="menu_icon">                                 
											<span class="menu-icon">
												<!-- <a href="<?php //echo get_bloginfo('ulr')?>/wishlist/" class="favorite"> -->
														<i class="fa fa-star" aria-hidden="true"></i>
													<!-- </a> -->
											</span>							
										     <a href="<?php echo get_bloginfo('url'); ?>/carrinho/" class="modal-cart-open">                                 
												<span class="menu-icon">
													<i class="fa fa-shopping-cart" aria-hidden="true">
													<?php if(WC()->cart->get_cart_contents_count()): ?>
														<div class="cart_itens_total">
															<?php echo WC()->cart->get_cart_contents_count(); ?>
														</div>		
													<?php endif; ?>
													</i>
												</span>
											 </a>										 
										</div>
										<?php
											global $woocommerce;
											$items = $woocommerce->cart->get_cart();	
										?>
										<?php if ($items){ ?>
										<div class="modal-cart">
											<button class="modal-cart-close">
												<i class="fa fa-remove"></i>
											</button>
											<ul class="modal-cart-items">
											<?php foreach($items as $item => $values): ?>
												<?php
													$produto = wc_get_product( $values['product_id'] );
												?>
												<li>
													<div class="cart-item-image">
														<?php echo $produto->get_image("full"); ?>
													</div>
													<div class="cart-item-description">
														<em class="cart-modal-title"><?php echo $produto->get_title(); ?></em>
														<span class="cart-modal-price">
															<?php
																echo $values["quantity"] ."x ". WC()->cart->get_product_subtotal( $produto, $values["quantity"]);
															?>
														</span>
													</div>
												</li>
											<?php endforeach ?>
											</ul>
											<ul class="modal-cart-total">
												<li>
													Subtotal: <span><?php echo wc_cart_totals_subtotal_html(); ?></span>
												</li>
											</ul>
											<nav>
												<a href="<?php echo wc_get_cart_url(); ?>" class="checkout-button button alt wc-forward wc-cart"><?php _e("Ver Carrinho", "vuelo"); ?></a>
												<?php do_action( 'woocommerce_proceed_to_checkout' ); ?>
											</nav>
										</div>
										<?php
											}
											else{
										?>
										<div class="modal-cart">
											<button class="modal-cart-close">
												<i class="fa fa-remove"></i>
											</button>
											<ul class="modal-cart-total">
												<li>
													<center><span>Sem produtos no carrinho</span></center>
												</li>
											</ul>
<!-- 											<nav>
												<a href="<?php echo wc_get_cart_url(); ?>" class="checkout-button button alt wc-forward wc-cart"><?php _e("Ver Carrinho", "vuelo"); ?></a>
												<?php do_action( 'woocommerce_proceed_to_checkout' ); ?>
											</nav> -->
										</div>										
										<?php												
											}
										?>								
									</div>
								</div>						
							</div>
						</div>
					</div>

					<div class="header-menu">
						<div class="relative">
							<div class="custom-menu">
								<div class="container">
									<div class="align_menu_interno">
										<div class="button <?php if(is_front_page()){echo 'home_button'; }else{echo 'internas_button';} ?>">
											<button type="button" class="button-responsive">			
												<?php for ($i=0; $i < 3; $i++) { ?>
														<svg version="1.1" id="Camas:xlink="http://www.w3.org/1999/xlink" x="0pda_1" xmlns="http://www.w3.org/2000/svg" xmlnx" y="0px"
															 width="30px" height="5px" viewBox="0 0 30 5" enable-background="new 0 0 30 5" xml:space="preserve">
															<path fill="#ffffff" d="M30,3.846C30,4.483,29.487,5,28.854,5H1.146C0.513,5,0,4.483,0,3.846V1.154C0,0.517,0.513,0,1.146,0h27.707
																C29.487,0,30,0.517,30,1.154V3.846z"/>
														</svg>			
												<?php } ?>
												<p class="compre">COMPRE POR MARCA</p>
											</button>					
										</div>						
										<div class="row">
											<div class="col-sm-12 sub">
												<nav>
													<ul class="principal">
														<li>
															<a><b>Compre por marca</b></a>
														</li>
														<li>
															<a href="<?php echo get_bloginfo('url'); ?>/loja/?swoof=1&product_cat=brother">Brother</a>
															<nav class="fadeInDown filho">
																<div class="relative">
																	<ul>
																		<li>
																			<a href="#">Cartucho de tinta</a>
								
																		<li>
																			<a href="#">toners</a>
																		</li>
																		<li>
																			<a href="#">Impressoras</a>
																		</li>
																		<li>
																			<a href="#">Peças</a>
																		</li>
																		<li>
																			<a href="#">Outros Departamentos</a>
																		</li>
																	</ul>
																	<div class="promocao">

																	</div>					
																</div>
															</nav>										
														</li>
														<li>
															<a href="<?php echo get_bloginfo('url'); ?>/loja/?swoof=1&product_cat=cannon">Cannon</a>
															<nav class="fadeInDown filho">
																<div class="relative">
																	<ul>
																		<li>
																			<a href="#">Cartucho de tinta</a>
								
																		<li>
																			<a href="#">toners</a>
																		</li>
																		<li>
																			<a href="#">Impressoras</a>
																		</li>
																		<li>
																			<a href="#">Peças</a>
																		</li>
																		<li>
																			<a href="#">Outros Departamentos</a>
																		</li>
																	</ul>		
																</div>
															</nav>									
														</li>
														<li>
															<a href="<?php echo get_bloginfo('url'); ?>/loja/?swoof=1&product_cat=epson">Epson</a>
															<nav class="fadeInDown filho">
																<div class="relative">
																	<ul>
																		<li>
																			<a href="#">Cartucho de tinta</a>
								
																		<li>
																			<a href="#">toners</a>
																		</li>
																		<li>
																			<a href="#">Impressoras</a>
																		</li>
																		<li>
																			<a href="#">Peças</a>
																		</li>
																		<li>
																			<a href="#">Outros Departamentos</a>
																		</li>
																	</ul>		
																</div>
															</nav>										
														</li>
														<li>
															<a href="<?php echo get_bloginfo('url'); ?>/loja/?swoof=1&product_cat=hp">Hp</a>
															<nav class="fadeInDown filho">
																<div class="relative">
																	<ul>
																		<li>
																			<a href="#">Cartucho de tinta</a>
								
																		<li>
																			<a href="#">toners</a>
																		</li>
																		<li>
																			<a href="#">Impressoras</a>
																		</li>
																		<li>
																			<a href="#">Peças</a>
																		</li>
																		<li>
																			<a href="#">Outros Departamentos</a>
																		</li>
																	</ul>		
																</div>
															</nav>										
														</li>
														<li>
															<a href="<?php echo get_bloginfo('url'); ?>/loja/?swoof=1&product_cat=lexmarc">Lexmarc</a>
															<nav class="fadeInDown filho">
																<div class="relative">
																	<ul>
																		<li>
																			<a href="#">Cartucho de tinta</a>
								
																		<li>
																			<a href="#">toners</a>
																		</li>
																		<li>
																			<a href="#">Impressoras</a>
																		</li>
																		<li>
																			<a href="#">Peças</a>
																		</li>
																		<li>
																			<a href="#">Outros Departamentos</a>
																		</li>
																	</ul>		
																</div>
															</nav>										
														</li>
														<li>
															<a href="<?php echo get_bloginfo('url'); ?>/loja/?swoof=1&product_cat=xerox">Xerox</a>
															<nav class="fadeInDown filho">
																<div class="relative">
																	<ul>
																		<li>
																			<a href="#">Cartucho de tinta</a>
								
																		<li>
																			<a href="#">toners</a>
																		</li>
																		<li>
																			<a href="#">Impressoras</a>
																		</li>
																		<li>
																			<a href="#">Peças</a>
																		</li>
																		<li>
																			<a href="#">Outros Departamentos</a>
																		</li>
																	</ul>		
																</div>
															</nav>										
														</li>
													</ul>
												</nav>
											</div>
										</div>										
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="face-action display_social">
						<a href="#" tagert="_blank">
							<div class="row">
								<div class="col-sm-4">
									<a href="https://www.facebook.com/TonereCartuchoemCuritiba/?notif_t=page_unpublish&notif_id=1492620838008891" target="_blank">
										<i class="fa fa-facebook" aria-hidden="true"></i>
									</a>
								</div>
							</div>							
						</a>
					</div>

					<div class="whats-action display_social">
						<div class="row">
							<div class="first">
							<p>41 | 9 0000 - 0000</p>
							</div>						
							<div class="last">
								<i class="fa fa-whatsapp" aria-hidden="true"></i>
							</div>
						</div>
					</div>

					<div class="skype-action display_social">
						<div class="row">
							<div class="first">
							<p>sac@tonercartucho.com.br</p>
							</div>						
							<div class="last">
								<i class="fa fa-skype" aria-hidden="true"></i>
							</div>
						</div>
					</div>							
					
					<div class="mobile_menu">
						<div id="menu_inferior">
							<div class="menu_mobile_div">
								<button class="hamburger"><i class="fa fa-bars" aria-hidden="true"></i></button>
								<button class="cross"><i class="fa fa-times" aria-hidden="true"></i></button>
							</div>
<!-- 							<div class="logo">
								<div class="align_logo">
									<a href='<?php echo get_bloginfo('url'); ?>' title='<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>' rel='home'>
										<img src="<?php echo get_bloginfo('url'); ?>/wp-content/themes/tonnereCartucho de tinta/_assets/img/logo-branca.png">
									</a>
								</div>
							</div> -->

							<div class="menu_icon_mobile">
							   	<a href="<?php echo wc_get_cart_url(); ?>">
							   		<span class="menu-icon">
							   			<i class="fa fa-shopping-cart" aria-hidden="true">
							   			<?php if(WC()->cart->get_cart_contents_count()): ?>
								 			<div class="cart_itens_total">
								 				<?php echo WC()->cart->get_cart_contents_count(); ?>		
								 			</div>		
								 		<?php endif; ?>
							   			</i>
							   		</span>
							   	</a>
							</div>
							<div class="opcoes_inferior">
								<form id="search-box" class="search-box-mobile">
									<input type="text" name="s" class="form-control" placeholder="<?php _e("O que você procura?", "vuelo"); ?>">
									<button type="submit" id="search-open"><i class="fa fa-search" aria-hidden="true"></i></button>
								</form>
								<!-- Menu Mobile -->
								<div class="menu-vuelo-menu-container">
									<ul id="vuelo-menu" class="nav navbar-nav text-center">
										<li class="menu-item">
											<a href="<?php echo site_url(); ?>">
												Inicio
											</a>
										</li>
										<li class="menu-item">
											<a href="<?php echo site_url(); ?>/loja/">Loja</a>
										</li>
										<li class="sobre menu-item">
											<a><b>COMPRE POR MARCA</b></a>
										</li>
										<li class="menu-item">
											<a href="<?php echo site_url(); ?>">
												Brother
											</a>
										</li>
										<li class="menu-item">
											<a href="<?php echo site_url(); ?>">
												Cannon
											</a>
										</li>									
										<li class="produtos-header menu-item menu-item-has-children">
											<a>Epson</a>
											<ul class="sub-menu" style="display: none;">
												<li class="menu-item">
													<a href="<?php echo site_url(); ?>/?swoof=1&product_cat=cartucho-de-tinta">
														Cartuchos de tinta
													</a>
												</li>
												<li class="menu-item">
													<a href="<?php echo site_url(); ?>/?swoof=1&product_cat=cartucho-de-tinta">
														Refil de tinta
													</a>
												</li>												
												<li class="menu-item">
													<a href="">
														Toners
													</a>
												</li>
												<li class="menu-item">
													<a href="">
														Impressoras
													</a>
												</li>
												<li class="menu-item">
													<a href="">
														Peças
													</a>
												</li>
												<li class="menu-item">
													<a href="">
														Outros Departamentos
													</a>
												</li>												
											</ul>
										</li>
										<li class="menu-item">
											<a href="<?php echo site_url(); ?>">
												Hp
											</a>
										</li>
										<li class="menu-item">
											<a href="<?php echo site_url(); ?>">
												Lexmarc
											</a>
										</li>
										<li class="menu-item">
											<a href="<?php echo site_url(); ?>">
												Xerox
											</a>
										</li>
									</ul>
								</div>
								<div class="menu_icon">
								   <span id="search" class="menu-icon">
								   		<i class="fa fa-star" aria-hidden="true"></i>
								   </span>

								   <span class="menu-icon">
								   	<i class="fa fa-shopping-cart" aria-hidden="true"></i>
								   </span> 
								</div>
							</div>
						</div>
					</div>
				</div>			
			</header>			
			<div id="content">
				<div class="scrolTop">
					<div style="position: relative;">
						<i class="fa fa-angle-up" aria-hidden="true"></i>
					</div>
				</div>		