<?php
	get_header();
	the_post();
?>

<div id="nav-bar">
	<p>
		<a href="<?php echo home_url(); ?>"><i class="fa fa-home"></i></a>
		>
		<span><?php the_title(); ?></span>
	</p>
</div>

<section class="page-default container">
	<h1 class="title"><?php the_title(); ?></h1>
	<article class="content-default">
		<?php the_content(); ?>
	</article>
</section>

<?php get_footer(); ?>
