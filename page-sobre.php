<?php get_header(); the_post();
?>

	<div id="nav-bar">
		<p><a href="<?php echo home_url(); ?>"><i class="fa fa-home" aria-hidden="true"></i></a> > <span><?php the_title(); ?></span></p>
	</div>

	<div id="sobre">
		<section id="section1">

				<?php the_content(); ?>
			
		    <div class="clearfix"></div>
		</section>		
	</div>


<?php get_footer(); ?>