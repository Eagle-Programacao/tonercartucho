
(function($) {
    var $wrapper = $('.oc-button__wrapper'),
        $inputs = $('.oc-button__form-inputs'),
        $response = $('.oc-button__form-response'),
        $name = $('.oc--name'),
        $phone = $('.oc--phone'),
        $msgPhone = $('.oc-button__form-response span'),
        isOpen = false;

    // Toggle Button
    function toggleButton() {
        isOpen = !isOpen;
        $wrapper.toggleClass('oc-button--open');

        if (isOpen) {
            $name.focus();
        }
    }

    // Form submit
    function submitForm(values) {

        if (!omnichatId) {
            var errorMessage = "A variÃ¡vel omnichatId nÃ£o esta configurada";
            $response.html("<p>" + errorMessage + "<p");
            console.error('OmniChat Button Error: ' + errorMessage);
            return;

        }

        var number = values.phoneNumber.substring(0, 2) + ' ' + values.phoneNumber.substring(2);
        $msgPhone.html(number);

        var data = {
            name: values.name,
            source: 'WHATSAPP',
            sourceId: '55' + values.phoneNumber,
            sourceDetail: window.location.href,
            retailerId: omnichatId
        };

        $inputs.fadeOut('600', function() {
            $response.html("<p>" + "Enviando sua solicitaÃ§Ã£o, por favor aguarde..." + "<p");
            $response.fadeIn('600', function() {
                $.ajax({
                    url: 'https://api.omni.chat/parse/functions/createCustomerSupport',
                    type: 'POST',
                    headers: {
                        'X-Parse-REST-API-Key': 'hF8g4ihvGYPFRQR3zOsdjIgwAYzt5GK7kNJxUCFj',
                        'X-Parse-Application-Id': 'UCeS99itvZg1tsea2OSoyKvpLbKddhoVAPotIQOy',
                        'Content-Type': 'text/plain; charset=utf-8'
                    },
                    processData: false,
                    data: JSON.stringify(data),
                }).done(function(answer) {
                    $response.fadeOut('600', function() {
                        answer.result = answer.result.replace("{customerPhone}", number);
                        answer.result = answer.result.replace("{customerName}", values.name);

                        $response.html("<p>" + answer.result + "<p");
                        $response.fadeIn('600');

                    });
                }).fail(function(error) {
                    $response.fadeOut('600', function() {
                        console.error('OmniChat Button Error: ' + JSON.stringify(error));
                        $response.html("<p> Ops... Houve um erro ao enviar sua solicitaÃ§Ã£o, por favor tente mais tarde.<p>");
                        $response.fadeIn('600');
                    });

                }).always(function() {
                    setTimeout(toggleButton, 8000);
                    setTimeout(function() {
                        $inputs.fadeIn('600');
                        $response.fadeOut('600');
                    }, 8400);
                });
            });
        });

    }

    // Input Mask
    $phone.inputmask({
        mask: ['(99) 9999-9999', '(99) 99999-9999'],
        autoUnmask: true,
        showMaskOnHover: false,
        showMaskOnFocus: false,
        oncomplete: function() {
            $phone.removeClass('oc--error');
        }
    });

    // Open Button
    $(document).on('click', '.oc-button__header', function() {
        toggleButton();
    });

    // Submit
    $(document).on('click', '.oc-button__button', function(e) {
        e.preventDefault();

        var name = $name.val(),
            phoneNumber = $phone.val();

        if (name.length > 0) {
            $name.removeClass('oc--error');
        } else {
            $name.addClass('oc--error');
        }

        if (phoneNumber.length >= 10) {
            $phone.removeClass('oc--error');
        } else {
            $phone.addClass('oc--error');
        }

        if (name.length > 0 && phoneNumber.length >= 10) {
            submitForm({
                name: name,
                phoneNumber: phoneNumber
            });
        }
    });
})(jQuery);