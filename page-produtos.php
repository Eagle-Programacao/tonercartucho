<?php get_header(); ?>

	<div id="content">
		<div id="nav-bar">
			<p><a href="<?php echo site_url(); ?>"><i class="fa fa-home" aria-hidden="true"></i></a> &gt; <span><?php _e("Produtos", "vuelo"); ?></span></p>
		</div>

	    <?php get_template_part("inc/produtos", "highlight"); ?>

	    <?php get_template_part("inc/banner", "highlight"); ?>
	    
	</div>


<?php get_footer(); ?>