<?php get_header(); ?>
<?php wp_reset_query(); ?>
<?php get_header( 'shop' ); 
?>
<section class="breadcrumb_loja">
	<div class="container">
		<?php
			do_action( 'woocommerce_before_main_content' );
		?>		
	</div>
</section>
<div class="title_loja">
	<div class="container">
		<h3>Loja</h3>
	</div>
</div>	
<section class="produtos">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3 filtro">
				<?php do_action( 'woocommerce_before_shop_loop' ); ?>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 produtos_area">
				<div class="row ordenacao">
					<?php do_action( 'woocommerce_before_shop_loop' ); ?>
				</div>
				<div class="row loop_produtos">
					<section class="Produtos mais-vendidos">
					    <div class="container">
					        <div class="row produtos_row">
					        <?php 
		    		        	$aux = 0;
					 			if ( have_posts() ) :
					 				while ( have_posts() ) : the_post();					                    
					                    if (has_post_thumbnail()) {
					                        $thumbnail = get_the_post_thumbnail(get_the_ID() , 'full');
					                        $thumbnail_url = $thumbnail_data[0];
					                    }
					                    if($aux == 0){echo "<div class='row'>";}else if($aux % 3 == 0){echo "</div><div class='row'>";}
					        ?>            
					                        <div class="col-lg-4 col-md-4 col-sm-4">
					                            <figure>    
					                                <a href="<?php the_permalink(); ?>">                                
					                                    <?php echo $thumbnail; ?>                                
					                                </a>
					                            </figure>
					                            <div class="info">
					                                <a href="<?php the_permalink(); ?>">
					                                    <h4><?php the_title(); ?></h4>
					                                </a>
                               							<p> <?php echo $product->get_price_html(); echo "<br>"; echo $product->is_in_stock() ? 'Em estoque' : 'Fora do estoque'; ?> </p> 
													<?php
														global $product;

														echo apply_filters( 'woocommerce_loop_add_to_cart_link',
															sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a>',
																esc_url( $product->add_to_cart_url() ),
																esc_attr( isset( $quantity ) ? $quantity : 1 ),
																esc_attr( $product->id ),
																esc_attr( $product->get_sku() ),
																esc_attr( isset( $class ) ? $class : 'btn-lg btn-block hvr-wobble-horizontal' ),
																esc_html( $product->add_to_cart_text() )
															),
														$product );
													?>						                               
					                            </div>
					                        </div>
					                    <?php $aux++; endwhile; ?>
					                <?php wp_reset_postdata(); ?>
					            <?php endif; ?>
					            <?php the_pagination(); ?>
					        </div>                      
					    </div>
					</section>
				</div>							
			</div>			
		</div>
	</div>
</section>
<?php get_footer(); ?>