		</div><!-- .site-content -->

		<script>
			jQuery.extend(jQuery.validator.messages, {
			    required: "<?php _e('Este campo é requerido.', 'vuelo'); ?>",
			    remote: "<?php _e('Por favor, corrija este campo.', 'vuelo'); ?>",
			    email: "<?php _e('Por favor, forneça um endereço eletrônico válido.', 'vuelo'); ?>",
			    url: "<?php _e('Por favor, forneça uma URL válida.', 'vuelo'); ?>",
			    date: "<?php _e('Por favor, forneça uma data válida.', 'vuelo'); ?>",
			    dateISO: "<?php _e('Por favor, forneça uma data válida (ISO).', 'vuelo'); ?>",
			    number: "<?php _e('Por favor, forneça um número válido.', 'vuelo'); ?>",
			    digits: "<?php _e('Por favor, forneça somente dígitos.', 'vuelo'); ?>",
			    creditcard: "<?php _e('Por favor, forneça um cartão de crédito válido.', 'vuelo'); ?>",
			    equalTo: "<?php _e('Por favor, forneça o mesmo valor novamente.', 'vuelo'); ?>",
			    accept: "<?php _e('Por favor, forneça um valor com uma extensão válida.', 'vuelo'); ?>",
			    maxlength: jQuery.validator.format("<?php _e('Por favor, forneça não mais que {0} caracteres.', 'vuelo'); ?>"),
			    minlength: jQuery.validator.format("<?php _e('Por favor, forneça ao menos {0} caracteres.', 'vuelo'); ?>"),
			    rangelength: jQuery.validator.format("<?php _e('Por favor, forneça um valor entre {0} e {1} caracteres de comprimento.', 'vuelo'); ?>"),
			    range: jQuery.validator.format("<?php _e('Por favor, forneça um valor entre {0} e {1}.', 'vuelo'); ?>"),
			    max: jQuery.validator.format("<?php _e('Por favor, forneça um valor menor ou igual a {0}.', 'vuelo'); ?>"),
			    min: jQuery.validator.format("<?php _e('Por favor, forneça um valor maior ou igual a {0}.', 'vuelo'); ?>")
			});
			var aguarde = ['<?php _e("AGUARDE", "vuelo"); ?>'];
		</script>
		
		<section class="informacoes_footer">
			<div class="container">
				<div class="row">
					<div class="col-lg-5 col-md-5 col-sm-5">
						<p>VOCÊ SABIA?</p>
						<h5>Aceitamos várias <b>formas de pagamento</b>. Escolha a sua:</h5>
						<br>
						<div class="row credito">
							<figure>
								<img src="<?php echo get_bloginfo('template_url');?>/_assets/img/icons/cartao-de-credito.jpg">
							</figure>							
						</div>
						<div class="row pagamento">
							<figure>
								<img src="<?php echo get_bloginfo('template_url');?>/_assets/img/icons/formas-de-pagamento.jpg">
							</figure>							
						</div>						
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4 mobal_footer_left">
						<div class="row title">
							<i class="fa fa-lock" aria-hidden="true"></i><p>COMPRA SEGURA</p>
						</div>
						<div class="row certificados">
							<figure>
								<img src="<?php echo get_bloginfo('template_url'); ?>/_assets/img/icons/certificados.jpg">
							</figure>
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 mobal_footer_left">
						<div class="row atendimento">
							<h5>CENTRAL DE ATENDIMENTO</h5>
							<p>Autoatendimento 24 Horas</p>
							<p>Segunda a sexta - feira das 8h30 às 18h.</p>
							<p>(exceto feriados)</p>						
						</div>
						<div class="row sociais">
							<a href="#">
								<a class="open_modal_whats"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
								<a href="https://www.facebook.com/TonereCartuchoemCuritiba/?notif_t=page_unpublish&notif_id=1492620838008891" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
								<a class="skype_click"><i class="fa fa-skype" aria-hidden="true"></i></a>
								<!-- <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a> -->
							</a>				
						</div>
					</div>															
				</div>
			</div>
		</section>

		<footer id="footer" class="site-footer" role="contentinfo">
			<div class="container">
				<div class="row footer_topo">
					<div class="col-lg-5 col-md-5 col-sm-5 no_padding_right">
						<div class="col-lg-6 col-md-6 col-sm-6">
							<h5>A TONERS & CARTUCHOS:</h5>
							<p><a href="">Sobre a Toners & Cartuchos</a></p>
							<p><a href="<?php echo get_bloginfo('url'); ?>/central-de-atendimento/">Dúvidas mais comuns</a></p>							
							<p><a href="<?php echo get_bloginfo('url'); ?>/central-de-atendimento/">Central de Atendimento</a></p>							
							<!-- <p><a href=" https://tonercartucho.com.br/responsabilidade-ambiental/">Meio Ambiente</a></p -->>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6">
							<h5>PARA COMPRAR:</h5>
							<p><a href="<?php echo get_bloginfo('url'); ?>/central-de-atendimento/">Formas de pagamento</a></p>
							<p><a href="">Segurança e Privacidade</a></p>
							<p><a href="">Contrato de compra e Venda</a></p>
						</div>			
					</div>
					<div class="col-lg-2 col-md-2 col-sm-2 no_padding_left">
						<h5>APÓS SUA COMPRA:</h5>
						<p><a href="<?php echo get_bloginfo('url'); ?>/minha-conta/">Acompanhe seu pedido</a></p>
						<p><a href="<?php echo get_bloginfo('url'); ?>/central-de-atendimento/">Troca ou devolução</a></p>				
					</div>
					<div class="col-lg-2 col-md-2 col-sm-2">
						<h5>COMPRE POR MARCA:</h5>
						<p><a href="">Brother</a></p>
						<p><a href="">Cannon</a></p>
						<p><a href="">Epson</a></p>
						<p><a href="">Hp</a></p>				
						<p><a href="">Lexmarc</a></p>				
						<p><a href="">Xerox</a></p>				
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 novidades">
						<div>
							<center><i class="fa fa-envelope-o" aria-hidden="true"></i></center>
							<center><h5>Cadastre-se e receba nossas novidades e Promoções :)</h5></center>
							<?php echo do_shortcode('[contact-form-7 id="64" title="Formulário de contato 1"]');?>
						</div>
					</div>
				</div>		
			</div>
			<div class="bottom_footer">
				<div class="container">									
						<p>® 2017 TCI Brasil comercio e manutenção de computadores e suprimentos de informática / CNPJ: 26.495.646/0001-80 / Inscrição Estadual: 90.735.605-16 </p>
						<p>Vendas realizadas somente pela internet / comercial@tonercartucho.com.br / Telefone: (41) 0000 - 0000 / Atendimento: Dias úteis das 8h30 às 18h.</p>
				</div>
			</div>
		</footer><!-- .site-footer -->
	</div>
</div><!-- .site -->

<script type="text/javascript">
	jQuery(".fechar_woo_action .fa-chevron-left").hide();
	jQuery(".fechar_woo_action div").click(function(){
			jQuery(".call_to_action_product").toggleClass("fadeOutRight animated");
	});

	jQuery( document ).ready(function(){
		jQuery(".open_modal_whats").click(function( event ){
			jQuery(".oc-button__wrapper").toggleClass("oc-button--open");
		});
	});

		jQuery("#scroll_desc").click(function(){
		 jQuery("body").animate({"scrollTop": window.scrollY+450}, 1000);
		        return false;
		});	
	
</script>

<div style="width: 200px; position: fixed; margin: 0px 10px 10px; z-index: 9999; right: 0px; bottom: 0px;">
<link rel="stylesheet" href="<?php echo get_bloginfo('template_url')?>/_assets/css/omni.css">
  <div class="oc-button__wrapper">
    <div class="oc-button__content">
      <div class="oc-button__header">
        <span>Informações pelo<br><strong>WhatsApp</strong><i></i></span>
       </div>
      <div class="oc-button__form">
        <div class="oc-button__form-inputs" style="display: block;">
          <input class="oc-button__input [ oc--name ]" type="text" placeholder="Nome">
          <input class="oc-button__input [ oc--phone ]" type="text" placeholder="ex.: (99) 9999-9999">
          <button class="oc-button__button">Solicitar atendimento</button>
        </div>
        <div class="oc-button__form-response" style="display: none;">
          <p></p>
        </div>
      </div>
    </div>
	  <script> var omnichatId = "iCr2Z4VOrD"</script>
	  <script src="<?php echo get_bloginfo('template_url')?>/_assets/js/input-mask.min.js"></script>
	  <script src="<?php echo get_bloginfo('template_url')?>/_assets/js/omni.js"></script>
</div>

<?php wp_footer(); ?>

</body>
</html>
