<?php get_header(); ?>

	<div id="nav-bar">
		<p><a href="<?php echo home_url(); ?>"><i class="fa fa-home" aria-hidden="true"></i></a> > <span><?php the_title(); ?></span></p>
	</div>

	<div id="contato">
		<section id="section1">
			<div class="col-md-6 img">
				<h2><?php _e("Como podemos ajudar?", "vuelo"); ?></h2>
			</div>
			<div class="col-md-6 form">
				<div class="title">
					<h1><?php _e("FALE CONOSCO", "vuelo"); ?></h1>
					<p><?php _e("Preencha o formulário abaixo. Retornaremos o mais breve possível.", "vuelo"); ?></p>
				</div>
				<form id='mailing-form' method="POST">
					<div class="form-group">
						<label>
							<?php switch(qtranxf_getLanguage())
							{
								case 'pt': echo "NOME"; break;
								case 'en': echo "NAME"; break;
								case 'es': echo "NOMBRE"; break;
							} ?>
						</label>
						<input class="form-control nome" type="text" id="nome_contato" required name="nome">
					</div>
					<div class="form-group">
						<label><?php _e("E-MAIL", "vuelo"); ?>:</label>
						<input class="form-control email" type="text" id="email_contato" required name="email">
					</div>
					<div class="alert alert-unsucces" id="unsuccess_form_email">
						<?php _e("Email inválido.", "vuelo"); ?>
					</div>
					<div class="form-group">
						<label><?php _e("TELEFONE", "vuelo"); ?>:</label>
						<input class="form-control" type="text" id="telefone_contato" required name="telefone">
					</div>

					<?php
						if(qtranxf_getLanguage() === 'pt')
						{
					?>
							<script type="text/javascript">
								jQuery('#telefone_contato').mask('(00) 0 0000-0000');
							</script>					
							<div class="form-group">
								<div class="col-sm-12 col-lg-6 select-state">
									<label><?php _e("ESTADO", "vuelo"); ?>:</label>
									<select class="form-control estado" id="estado" required name="estado"></select>
									<i class="fa fa-angle-down" aria-hidden="true"></i> 
								</div>
								<div class="col-sm-12 col-lg-6 select-city">
									<label><?php _e("CIDADE", "vuelo"); ?>:</label>
									<select class="form-control cidade" id="cidade" required name="cidade"></select>
									<i class="fa fa-angle-down" aria-hidden="true"></i>
								</div>
								<div class="clearfix"></div>
							</div>
					<?php
						}
						else
						{
					?>
					<script type="text/javascript">
						jQuery('#telefone_contato').mask('0000-00000000');
					</script>						
					<style>
						#contato #section1 .form #mailing-form .select-city
						{
							padding-left: 0px;
						}
					</style>
							<div class="form-group">
								<div class="col-sm-12 col-lg-4 select-pais">
									<label>
										<?php 
											switch(qtranxf_getLanguage())
											{
												case 'en': echo 'COUNTRY'; break;
												case 'es': echo 'PAÍS'; break;
												case 'pt': echo 'PAÍS'; break;
											}											
										?>
									</label>
									<?php require("inc/select-paises-formContato.php"); ?>
									<i class="fa fa-angle-down" aria-hidden="true"></i>
								</div>							
								<div class="col-sm-12 col-lg-4 select-state">
									<label><?php _e("ESTADO", "vuelo"); ?>:</label>
									<select class="form-control estado" id="estado" required name="estado"></select>
									<i class="fa fa-angle-down" aria-hidden="true"></i>
								</div>
								<div class="col-sm-12 col-lg-4 select-city">
									<label><?php _e("CIDADE", "vuelo"); ?>:</label>
									<select class="form-control cidade" id="cidade" required name="cidade"></select>
									<i class="fa fa-angle-down" aria-hidden="true"></i>
								</div>
								<div class="clearfix"></div>
							</div>
					<?php
						}
					?>

					<div class="form-group">
						<label><?php _e("MENSAGEM", "vuelo"); ?>:</label>
						<textarea class="form-control mensagem" id="mensagem_contato" required name="mensagem"></textarea> 
					</div>
					<div class="form-group" id="loadButton">
						<button id="mailing-send" name="submit" value="enviar" class="pink-button btn-lg btn-block hvr-wobble-horizontal"> <?php _e("ENVIAR", "vuelo"); ?></button>
						<div style="display:none;" id='lista-integracao-sucessmsg'></div>
					</div>
				</form>

				<div class="alert alert-success" id="success-form" style="display:none">
					<?php _e("Enviamos o contato.", "vuelo"); ?>
				</div>

				<div class="alert alert-unsucces" id="unsuccess-form" style="display:none">
					<?php _e("Erro: O contato não foi enviado com sucesso.", "vuelo"); ?>
				</div>


			</div>
			<div class="icon-ball"><i class="fa fa-comments" aria-hidden="true"></i></div>
			<div class="clearfix"></div>
		</section>
		<section id="section2">
			<div class="row">	
				<div class="align_coluns">
					<div class="row">
						<div id="col-1" class="col-md-3">
							<div class="where">
								<div class="icon-ball"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
								<h1><?php _e("ONDE ESTAMOS", "vuelo"); ?></h1>
							</div>
						</div>
						<div id="col-2" class="col-md-3">
							<div>
								<h3><?php _e("Administrativo", "vuelo"); ?></h3>
								<p>Rua Antônio Rodrigues, 637</p>
								<p>Seminário, Curitiba - PR - CEP: 80740-560</p>
								<p>+55 (41) 3657-7611</p>
								<p>contato@vuelopharma.com.br</p>
							</div>
						</div>
						<div id="col-3" class="col-md-3">
							<div>
								<h3><?php _e("Fábrica", "vuelo"); ?></h3>
								<p>Rua Antonio de Paula Braga, 83</p>
								<p>Almirante Tamandaré</p>
								<p>Região Metropolitana de Curitiba, PR.</p>
							 	<p>CEP: 83506-010</p>
							</div>
						</div>
					</div>
						<div id="col-4" class="col-md-3">
							<div>
								<h3><?php _e("TRABALHE CONOSCO", "vuelo"); ?></h3>
								<p><?php _e("Tem interesse em trabalhar conosco? Envie seu currículo.", "vuelo"); ?></p>
								<button class="btn-lg btn-block hvr-wobble-horizontal" data-toggle="modal" data-target="#modal-curriculo"><?php _e("ENVIAR CURRÍCULO", "vuelo"); ?></button>

							</div>
						</div>
					</div>		
				</div>				
		</section>
	</div>

	<script type="text/javascript" charset="utf-8" src="<?php bloginfo("template_url"); ?>/_assets/js/cidades-estados-v0.2.js"></script> 
	<script type="text/javascript">
	    window.onload = function() {
	        new dgCidadesEstados( 
	            document.getElementById('estado'), 
	            document.getElementById('cidade'), 
	            true
	        );
	    }    


   
	</script>

<?php get_footer(); ?>