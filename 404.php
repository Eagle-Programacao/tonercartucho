<?php
get_header(); ?>
	<div id="primary" class="content-area">
		<div class="align_404">
			<main id="main" class="site-main" role="main">
			<center><img src="<?php echo get_bloginfo('template_url'); ?>/_assets/img/svg_404.svg"></center>
				<section class="error-404 not-found">

					<header class="page-header">
						<h1 class="page-title"><?php _e( 'ESSA PÁGINA NÃO PÔDE SER ENCONTRADA', 'vuelo' ); ?></h1>
					</header><!-- .page-header -->

					<div class="page-content">
						<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'vuelo' ); ?></p>
						<center>
							<form method="GET" action="<?php bloginfo("url") ?>">
								<input type="text" name="s">
								<!-- <input type="hidden" name="post_type" value="post"> -->
								<button type="submit"><?php _e( 'Buscar', 'vuelo' ); ?></button>
							</form>
						</center>
					</div><!-- .page-content -->
				</section><!-- .error-404 -->

			</main><!-- .site-main -->
		</div>
	</div><!-- .content-area -->

<?php get_footer(); ?>
