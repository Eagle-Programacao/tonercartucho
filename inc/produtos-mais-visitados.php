<section class="Produtos mais-vendidos">
    <h2 class="title"><div class="container">Mais Visitados</div></h2>
    <div class="container">
        <div class="row"> 
        <?php 
            $aux = 0;
            $params = array(
                'post_type' => 'product',
                'posts_per_page' => 4,
                'order' => 'ASC',
                'product_cat' => 'os-mais-visitados'
            );

            $wc_query = new WP_Query($params);

            if ($wc_query->have_posts()):
                while ($wc_query->have_posts()):
                    $wc_query->the_post();
                    $aux++;
                    if (has_post_thumbnail()) {
                        $thumbnail = get_the_post_thumbnail(get_the_ID() , 'full');
                        $thumbnail_url = $thumbnail_data[0];
                    }
                    if($aux == 0){echo "<div class='row'>";}else if($aux % 5 == 0){echo "</div><div class='row'>";}
        ?>            
                        <div class="col-lg-3 col-md-3 col-sm-3 <?php if($aux % 4 == 0){echo 'border-right-none'; } ?>">
                            <figure>    
                                <a href="<?php the_permalink(); ?>">                                
                                    <?php echo $thumbnail; ?>                                
                                </a>
                            </figure>
                            <div class="info">
                                 <a href="<?php the_permalink(); ?>">
                                    <h4>
                                        <?php
                                            the_title();
                                        ?>                                        
                                    </h4>
                                </a>
                                <div id="star_home">
                                    <div class="align_star_home">
                                        <?php if(function_exists("kk_star_ratings")) : echo kk_star_ratings(get_the_ID()); endif; ?>
                                    </div>
                                    <div class="avaliacoes_absolute"></div>
                                </div>                                
                                <p> <?php echo $product->get_price_html(); echo "<br>"; echo $product->is_in_stock() ? 'Em estoque' : 'Fora do estoque'; ?> </p>                                
                                    <?php
                                        global $product;

                                        echo apply_filters( 'woocommerce_loop_add_to_cart_link',
                                            sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a>',
                                                esc_url( $product->add_to_cart_url() ),
                                                esc_attr( isset( $quantity ) ? $quantity : 1 ),
                                                esc_attr( $product->id ),
                                                esc_attr( $product->get_sku() ),
                                                esc_attr( isset( $class ) ? $class : 'btn-lg btn-block hvr-wobble-horizontal' ),
                                                esc_html( $product->add_to_cart_text() )
                                            ),
                                        $product );
                                    ?>
                            </div>
                        </div>                          
                    <?php endwhile; ?>
                <?php wp_reset_postdata(); ?>
            <?php endif; ?>  
        </div>                      
    </div>
</section>