<?php 

//Adiciona opção Faq
$labels = array(
    'name'                => "FAQ",
    'singular_name'       => "FAQ",
    'menu_name'           => "FAQ",
    'name_admin_bar'      => "FAQ",
);
$args = array(
    'labels'              => $labels,
    'supports'            => array('title','editor','thumbnail'),
    'hierarchical'        => false,
    'public'              => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'menu_position'       => 25,
    'menu_icon'           => 'dashicons-format-status',
    'show_in_admin_bar'   => true,
    'show_in_nav_menus'   => true,
    'can_export'          => true,
    'has_archive'         => true,
    'exclude_from_search' => true,
    'publicly_queryable'  => true,
    'capability_type'     => 'post',
);
register_post_type( 'faq', $args );

// Register Faq Taxonomy
function faq_taxonomy() {

  $labels = array(
    'name'                       => _x( 'Categorias', 'Taxonomy General Name', 'text_domain' ),
    'singular_name'              => _x( 'Categorias', 'Taxonomy Singular Name', 'text_domain' ),
    'menu_name'                  => __( 'Categorias', 'text_domain' ),
    'all_items'                  => __( 'Todos os Items', 'text_domain' ),
    'parent_item'                => __( 'Parent Item', 'text_domain' ),
    'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
    'new_item_name'              => __( 'New Item Name', 'text_domain' ),
    'add_new_item'               => __( 'Add Nova Categoria', 'text_domain' ),
    'edit_item'                  => __( 'Editar Item', 'text_domain' ),
    'update_item'                => __( 'Update Item', 'text_domain' ),
    'view_item'                  => __( 'View Item', 'text_domain' ),
    'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
    'add_or_remove_items'        => __( 'Add ou remover items', 'text_domain' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
    'popular_items'              => __( 'Popular Items', 'text_domain' ),
    'search_items'               => __( 'Procurar Items', 'text_domain' ),
    'not_found'                  => __( 'Não Encontrado', 'text_domain' ),
    'no_terms'                   => __( 'Nenhum item', 'text_domain' ),
    'items_list'                 => __( 'Items list', 'text_domain' ),
    'items_list_navigation'      => __( 'Items list navigation', 'text_domain' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
  );
  register_taxonomy( 'taxonomy_faq', array( 'faq' ), $args );

}
add_action( 'init', 'faq_taxonomy', 0 );

?>