var	gulp       = require('gulp'),
	concat     = require('gulp-concat'),
	uglify     = require('gulp-uglify'),
	rename     = require('gulp-rename'),
	sass       = require('gulp-sass'),
	bless      = require('gulp-bless'),
	livereload = require('gulp-livereload'),
	gulpUtil   = require('gulp-util');

var assetsFolder = './_assets/';

var config = {
	scripts: [
		(assetsFolder + 'js/plugins/core/*.js'),
		(assetsFolder + 'js/plugins/*.js'),
		(assetsFolder + 'js/app/*.js'),
		(assetsFolder + 'js/app/**/*.js')
	],
	sass: [
		(assetsFolder + 'sass/**/*.scss')
	],
	sassDest: (assetsFolder + 'css/'),
	scriptDest: (assetsFolder + 'js/')
};

gulp.task('scripts', function() {
	return gulp.src(config.scripts)
		.pipe(uglify().on('error', function(error) {
			gulpUtil.log(
				error.toString()
				.replace(/\/.*?\/(themes)\/.+?\//gi,'')
				.replace(/: syntaxerror/gi,':\n\t'+gulpUtil.colors.red.bold.underline('SyntaxError'))
				.replace(/(filename:.+\/)(.+)/gi,'$1'+gulpUtil.colors.magenta.bold.underline('$2'))
				.replace(/(linenumber: )(\d+)/gi,'$1'+gulpUtil.colors.red.bold.underline('$2'))
			);
			this.emit('end');
		}))
		.pipe(concat('app.min.js'))
		.pipe(gulp.dest(config.scriptDest))
		.pipe(livereload());
});

gulp.task('sass', function () {
	return gulp.src(assetsFolder+'sass/style.scss')
		.pipe(sass.sync({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(gulp.dest(config.sassDest));
});

gulp.task('minify-css', ['sass'], function () {
	return gulp.src([
			assetsFolder+'css/*.css',
			'!'+assetsFolder+'css/all*min*.css'])
		.pipe(concat('all.min.css'))
		.pipe(bless())
		.pipe(sass.sync({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(gulp.dest(config.sassDest))
		.pipe(livereload());
});

gulp.task('watch', function () {
	livereload.listen(35729);
	gulp.watch('../**/*.php').on('change', function(file) {
		livereload.changed(file.path);
	});
	gulp.watch(config.sass, ['sass','minify-css']);
	gulp.watch(assetsFolder+'js/*/*.js', ['scripts']);
});

gulp.task('default', ['sass', 'minify-css', 'scripts', 'watch']);
