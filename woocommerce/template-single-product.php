<?php
	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly
	}

	global $post, $product, $aux_bread;


	if ( post_password_required() ) {
		echo get_the_password_form();
		return;
	}
?>

<div itemscope id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
		<meta itemprop="price" content="<?php echo esc_attr( $product->get_display_price() ); ?>" />
		<meta itemprop="priceCurrency" content="<?php echo esc_attr( get_woocommerce_currency() ); ?>" />
		<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />
	</div>

	<section class="section-produtos gelificador">
		<aside class="produto-content">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 gallery">
						<?php
							global $post;
						?>
							<?php						
								$thumb_id = get_post_thumbnail_id();
								$thumb_url = wp_get_attachment_url( $thumb_id );								
            				?>
            					<div class="tab-content">
            						<div class="tab-pane zoom zoom_active zoomIn animated active" id="image0" role="tabpanel">
            							<img class="zoom-shop" src="<?php echo $thumb_url; ?>" data-zoom-image="<?php echo $thumb_url; ?>">
            						</div>
	            					<?php
										$attachment_ids = $product->get_gallery_image_ids();
										$tab_um = 1;
										if ( $attachment_ids && has_post_thumbnail() ) {
											foreach ( $attachment_ids as $attachment_id ) {

												$thumb_url_tab = wp_get_attachment_url( $attachment_id );

												$html  = '<div class="tab-pane  zoom zoom_active zoomIn animated" id="image'.$tab_um++.'" role="tabpanel">';
												$html .= "<img src='".$thumb_url_tab."' data-zoom-image='".$thumb_url_tab."'>";
										 		$html .= '</div>';

												echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $attachment_id );
											}
										}							
									?>
							</div>
							<ul class="nav nav-tabs" role="tablist">
							  <li class="nav-item active">
							    <a class="nav-link active" data-toggle="tab" href="#image0" role="tab">
							    	<img id="zoom" src="<?php echo $thumb_url; ?>" data-zoom-image="<?php echo $thumb_url; ?>">
							    </a>
							  </li>
							<?php								
								$attachment_ids = $product->get_gallery_image_ids();
								$tab_dois = 1;
								if ( $attachment_ids && has_post_thumbnail() ) {
									foreach ( $attachment_ids as $attachment_id ) {	

										$thumb_url_tab = wp_get_attachment_url( $attachment_id );										
										$html  = ' <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#image'.$tab_dois++.'" role="tab">';
										$html .= '<img src="'.$thumb_url_tab.'">';
								 		$html .= '</a></li>';
										echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $attachment_id );
									}
								}							
							?>
							</ul>						
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6">
						<article class="produto-description">
							<div class="fechar_woo_action">
								<div>
									<i class="fa fa-chevron-right" aria-hidden="true"></i>
									<i class="fa fa-chevron-left" aria-hidden="true"></i>
								</div>
							</div>						
							<div class="call_to_action_product fadeIn">
								<h1 class="produto-title">
									<p class="woo_title">
										<?php
											$count = count($aux_bread) - 1;
											for($i = 0; $i <= count($aux_bread); $i++){
												if($i == count($aux_bread) - 1){
													echo $aux_bread[$i];
												}else if($i < count($aux_bread) - 1){
													echo $aux_bread[$i]." / ";
												}
											}								
										?>								
									</p>
									<div class="row comprar">
										<div class="col-lg-12 col-md-12 col-sm-12">
											<p> <?php echo $product->get_price_html(); echo "<br>"; echo $product->is_in_stock() ? 'Em estoque' : 'Fora do estoque'; ?> </p>
										</div>									
									</div>
									<div class="row desc_produto">
										<div class="col-lg-12 col-md-12 col-sm-12">
			                                <?php
				                                global $product;
					                                echo apply_filters( 'woocommerce_loop_add_to_cart_link',
					                                    sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">
						                                    	<div class="col-lg-2 col-md-2 col-sm-2">
							                                    		<i class="fa fa-cart-plus" aria-hidden="true"></i>
							                                    </div>
							                                    <div class="col-lg-10 col-md-10 col-sm-10">
							                                    	%s<br><span>Adicionar ao carrinho</span>
							                                    </div>
					                                    	</a>',
					                                        esc_url( $product->add_to_cart_url() ),
					                                        esc_attr( isset( $quantity ) ? $quantity : 1 ),
					                                        esc_attr( $product->id ),
					                                        esc_attr( $product->get_sku() ),
					                                        esc_attr( isset( $class ) ? $class : 'solid hvr-wobble-horizontal comprar_woocommerce' ),
					                                        esc_html( $product->add_to_cart_text() )
					                                    ),
					                                $product );
				                                ?>									
										</div>
									</div>
								</h1>
							</div>
							<h1 class="produto-title">					
								<?php
								$count = count($aux_bread) - 1;
									for($i = 0; $i <= count($aux_bread); $i++){
										if($i == count($aux_bread) - 1){
											echo $aux_bread[$i];
										}else if($i < count($aux_bread) - 1){
											echo $aux_bread[$i]." / ";
										}
									}								
								?>
								<div class="row comprar">
									<div class="col-lg-3 col-md-3 col-sm-3">
										<p> <?php echo $product->get_price_html(); echo "<br>"; echo $product->is_in_stock() ? 'Em estoque' : 'Fora do estoque'; ?> </p>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-4">
									</div>
									<div class="col-lg-5 col-md-5 col-sm-5">
									</div>
								</div>
								<div class="row desc_produto">
									<div class="col-lg-4 col-md-4 col-sm-5">
										<a id="scroll_desc" class="hvr-wobble-horizontal">Confira a descrição</a>
									</div>
									<div class="col-lg-3 col-md-2 .hidden-lg-down">
									</div>
									<div class="col-lg-5 col-md-6 col-sm-7">
		                                <?php
			                                global $product;
				                                echo apply_filters( 'woocommerce_loop_add_to_cart_link',
				                                    sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">
					                                    	<div class="col-lg-2 col-md-2 col-sm-2">
						                                    		<i class="fa fa-cart-plus" aria-hidden="true"></i>
						                                    </div>
						                                    <div class="col-lg-10 col-md-10 col-sm-10">
						                                    	%s<br><span>Adicionar ao carrinho</span>
						                                    </div>
				                                    	</a>',
				                                        esc_url( $product->add_to_cart_url() ),
				                                        esc_attr( isset( $quantity ) ? $quantity : 1 ),
				                                        esc_attr( $product->id ),
				                                        esc_attr( $product->get_sku() ),
				                                        esc_attr( isset( $class ) ? $class : 'solid hvr-wobble-horizontal comprar_woocommerce' ),
				                                        esc_html( $product->add_to_cart_text() )
				                                    ),
				                                $product );
			                                ?>									
									</div>
								</div>
							</h1><div id="desc" style="visibility: hidden;"></div>
							<div class="row calcular_frete">										
								<div class="col-lg-4 col-md-5 col-sm-8">
									<a href="<?php echo get_bloginfo('template_url').'/carrinho/#Calcular_Frete'; ?>" class="hvr-wobble-horizontal">Calcule o valor do frete</a>
								</div>
								<div class="col-lg-4 col-md-3 col-sm-4"></div>
								<div class="col-lg-4 col-md-4 col-sm-4">							
								</div>
							</div>
							<div class="row indique">
								<div class="col-lg-9 col-md-9 col-sm-10">
									<p>Gostou? Indique este produto ou receba nossas novidades</p>
									<a class="hvr-wobble-horizontal indique produto_novidades"><!-- <i class="fa fa-envelope-o" aria-hidden="true"> --></i>Receba nossas novidades</a>
									<div class="sociais">
										<a href="#">
											<a class="open_modal_whats"><i class="fa fa-whatsapp" aria-hidden="true"></i></a>
											<a href="https://www.facebook.com/TonereCartuchoemCuritiba/?notif_t=page_unpublish&notif_id=1492620838008891" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
											<a class="skype_click"><i class="fa fa-skype" aria-hidden="true"></i></a>
											<!-- <a><i class="fa fa-google-plus" aria-hidden="true"></i></a> -->
										</a>
									</div>
								</div>
								<div class="col-lg-3 col-md-3 .hidden-lg-down"></div>
								<div class="col-lg-2 col-md-2 col-sm-2">							
								</div>
							</div>							
						</article>
					</div>
				</div>
			</div>
		</aside>
		<aside class="sobre_o_produto">			
			<h2 class="title2">
				<div class="container">Sobre esse Produto</div>
			</h2>
			<div class="container">
				<div class="row description">
					<ul class="nav nav-tabs" role="tablist">
					  <li class="nav-item active">
					    <a class="nav-link active" data-toggle="tab" href="#caracteristicas" role="tab">Características Técnicas</a>
					  </li>
					  <li class="nav-item">
					    <a class="nav-link" data-toggle="tab" href="#descricao" role="tab">Descrição do Produto</a>
					  </li>
					  <li class="nav-item instrucoes">
					    <a class="nav-link" data-toggle="tab" href="#instrucoes_de_uso" role="tab">
					    	<i class="fa fa-medkit"></i></i>Indicações de Uso
					    </a>
					  </li>
					  <li class="nav-item compativeis">
					    <a class="nav-link" data-toggle="tab" href="#produtos_compativeis" role="tab">
					    	Produtos Compatíveis
					    </a>
					  </li>						  
					</ul>

					<div class="tab-content">
					  <div class="tab-pane caracteristicas active" id="caracteristicas" role="tabpanel">
					  	<h4><?php the_title(); ?></h4>
					  	<?php
					  		if(get_field("caracteristicas_tecnicas", get_the_ID()) != null)
					  		{
					  			echo get_field("caracteristicas_tecnicas", get_the_ID());
					  		}else
					  		{
					  			echo "Características Técnicas indisponíveis para este produto.";
					  		}						  						  		
					  	?>
					  </div>
					  <div class="tab-pane" id="descricao" role="tabpanel">
					  	<h4><?php the_title(); ?></h4>
					  	<?php					  		
					  		if(get_field("descrição_detalhada_do_produto", get_the_ID()) != null)
					  		{
					  			echo get_field("descrição_detalhada_do_produto", get_the_ID());
					  		}else
					  		{
					  			echo "Descrição indisponível para este produto.";
					  		}					  		
					  	?>
					  </div>
					  <div class="tab-pane" id="instrucoes_de_uso" role="tabpanel">
					  	<h4><?php the_title(); ?></h4>
					  	<?php
					  		if(get_field("indicacoes_de_uso", get_the_ID()) != null)
					  		{
					  			echo get_field("indicacoes_de_uso", get_the_ID());
					  		}else
					  		{
					  			echo "Indicações de Uso indisponíveis para este produto.";
					  		}
					  	?>
					  </div>
					  <div class="tab-pane" id="produtos_compativeis" role="tabpanel">
					  	<h4><?php the_title(); ?></h4>
					  	<?php
					  		if(get_field("produtos_compativeis", get_the_ID()) != null)
					  		{
					  			echo get_field("produtos_compativeis", get_the_ID());
					  		}else
					  		{
					  			echo "Sem Produtos compatíveis disponíveis para este produto.";
					  		}
					  	?>
					  </div>					  
					</div>					
				</div>
			</div>
		</aside>
		<aside class="produtos_relacionados">
			<section class="Produtos mais-vendidos">
			    <h2 class="title"><div class="container">Produtos Relacionados</div></h2>
			    <div class="container">
			        <div class="row">
						<?php echo do_shortcode('[recent_products per_page="4" columns="4"]');?>
			        </div>                      
			    </div>
			</section>        					
		</aside>
	</section>
	
	<meta itemprop="url" content="<?php the_permalink(); ?>" />
</div>
<section class="comments_area">
	<h2 class="title"><div class="container">Avaliações deste Produto</div></h2>
	<div class="container">
		<div class="row">
			<h3>Avalie este produto</h3>
			<?php
			if ( is_user_logged_in() ) {
			?>		
				<h4>Em quantas estrelas você avalia este produto?</h4>
				<p>1 - Péssimo | 2 - Ruim | 3 - Bom | 4 - Ótimo | 5 - Excelente</p>
				<p class="avaliar"> <?php if(function_exists("kk_star_ratings")) : echo kk_star_ratings(get_the_ID()); endif; ?></p><br><br><br>
				<h4>Gostou deste produto? Digite sua opnião abaixo.</h4>
			<?php
			}
			?>			
			<?php comment_form(); ?>
		</div>
		<div class="row avaliacoes">
			<br>
			<h3>Avaliações</h3>
			<?php comments_template(); ?>
		</div>
	</div>
</section> 