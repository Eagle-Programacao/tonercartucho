<?php
	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly
	}

	global $product;

	do_action( 'woocommerce_before_single_product' );

	if ( post_password_required() ) {
		echo get_the_password_form();
		return;
	}
?>

<div itemscope id="product-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
		<meta itemprop="price" content="<?php echo esc_attr( $product->get_display_price() ); ?>" />
		<meta itemprop="priceCurrency" content="<?php echo esc_attr( get_woocommerce_currency() ); ?>" />
		<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />
	</div>

	<section class="section-produtos gelificador">

		<nav class="nav-highlight">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-9 col-lg-7">
					<div class="row">
						<div class="col-sm-6">
							<div class="button relative_button">
								<button type="button" class="button-responsive">			
									<?php for ($i=0; $i < 3; $i++) { ?>
											<svg version="1.1" id="Camas:xlink="http://www.w3.org/1999/xlink" x="0pda_1" xmlns="http://www.w3.org/2000/svg" xmlnx" y="0px"
												 width="30px" height="5px" viewBox="0 0 30 5" enable-background="new 0 0 30 5" xml:space="preserve">
												<path fill="#ffffff" d="M30,3.846C30,4.483,29.487,5,28.854,5H1.146C0.513,5,0,4.483,0,3.846V1.154C0,0.517,0.513,0,1.146,0h27.707
													C29.487,0,30,0.517,30,1.154V3.846z"/>
											</svg>			
									<?php } ?>
									<p class="compre">COMPRE POR MARCA</p>
								</button>					
							</div>								
						</div>
						<div class="col-sm-6"></div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-3 col-lg-5">
					<ol> 
                        <li>
							<?php
							global $product;

							echo apply_filters( 'woocommerce_loop_add_to_cart_link',
								sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a>',
									esc_url( $product->add_to_cart_url() ),
									esc_attr( isset( $quantity ) ? $quantity : 1 ),
									esc_attr( $product->id ),
									esc_attr( $product->get_sku() ),
									esc_attr( isset( $class ) ? $class : 'solid hvr-wobble-horizontal comprar_woocommerce' ),
									esc_html( $product->add_to_cart_text() )
								),
							$product );
							?>	                        	
                        </li>
					</ol>
				</div>	
			</div>
		</nav>

		<aside class="produto-content">
			<div class="container">
				<div class="row">
					<div class="col-md-5 gallery">
						<?php
							global $post;
						?>
						<div class="images">
							<?php
								if ( has_post_thumbnail() ) {
									$attachment_count = count( $product->get_gallery_attachment_ids() );
									$gallery          = $attachment_count > 0 ? '[product-gallery]' : '';
									$props            = wc_get_product_attachment_props( get_post_thumbnail_id(), $post );
									$image            = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
										'title'	 => $props['title'],
										'alt'    => $props['alt'],
									) );
									echo apply_filters(
										'woocommerce_single_product_image_html',
										sprintf(
											'<a href="%s" itemprop="image" class="woocommerce-main-image zoom" title="%s" data-rel="prettyPhoto%s">%s</a>',
											esc_url( $props['url'] ),
											esc_attr( $props['caption'] ),
											$gallery,
											$image
										),
										$post->ID
									);
								} else {
									echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), __( 'Placeholder', 'woocommerce' ) ), $post->ID );
								}

								do_action( 'woocommerce_product_thumbnails' );
							?>
						</div>

					</div>
					<div class="col-md-7">
						<article class="produto-description">
							<h1 class="produto-title">
								<?php the_title(); ?>
									<p> <?php echo $product->get_price_html(); echo $product->is_in_stock() ? 'Em estoque' : 'Fora do estoque'; ?> </p>
							</h1>

							<p><?php the_content(); ?></p>
							<dl>
								<dt><i class="fa fa-medkit"></i> <?php _e("INDICAÇÕES DE USO", "vuelo"); ?></dt>
								<dd>
									<p><?php echo get_field("indicacoes_de_uso", get_the_ID()); ?></p>
								</dd>
							</dl>

							<dl>
								<dt><i class="fa fa-info-circle"></i> <?php _e("INSTRUÇÕES DE USO", "vuelo"); ?></dt>
								<dd>
									<p><?php echo get_field("instrucoes_de_uso", get_the_ID()); ?></p>
								</dd>
							</dl>
						</article>
					</div>
				</div>
			</div>
		</aside>
	</section>
	
	<meta itemprop="url" content="<?php the_permalink(); ?>" />

</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>