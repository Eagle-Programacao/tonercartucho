<?php

	if ( ! defined( 'ABSPATH' ) ) {
		exit; // Exit if accessed directly
	}

	get_header( 'shop' );

		do_action( 'woocommerce_before_main_content' );
	
		while ( have_posts() ) : the_post();
					wc_get_template_part( 'template', 'single-product' );
		endwhile;

	do_action( 'woocommerce_after_main_content' );

	 get_footer( 'shop' );

 ?>
