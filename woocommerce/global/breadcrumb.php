<div class="container">
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
global $aux_bread;
$aux_cont = 0;
if ( ! empty( $breadcrumb ) ) {

	echo $wrap_before;

	foreach ( $breadcrumb as $key => $crumb ) {

		echo $before;

		if ( ! empty( $crumb[1] ) && sizeof( $breadcrumb ) !== $key + 1 ) {
			$label = (esc_html( $crumb[0] ) == "Início") ? '<i class="fa fa-home"></i>' : esc_html( $crumb[0] );
			echo '<a href="'.get_bloginfo("url").'/?swoof=1&product_cat='.$crumb[0].'">' . $label . '</a>';
		} else {
			echo esc_html( $crumb[0] );
		}

		echo $after;

		if ( sizeof( $breadcrumb ) !== $key + 1 ) {
			echo '<span> <i class="fa fa-angle-right"></i> </span>';
		}
		if(esc_html( $crumb[0] ) == "Início"){
			$aux_bread[0] = "";
		}else{
			$aux_bread[$aux_cont++] = esc_html( $crumb[0] );
		}		
	}

	echo $wrap_after;

}
?>
</div>