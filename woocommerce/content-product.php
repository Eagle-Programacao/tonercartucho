<?php
    global $post, $product;
    $aux++;
    if (has_post_thumbnail()) {
        $thumbnail = get_the_post_thumbnail(get_the_ID() , 'full');
        $thumbnail_url = $thumbnail_data[0];
    }
    if($aux == 0){echo "<div class='row'>";}else if($aux % 5 == 0){echo "</div><div class='row'>";}
?>
    <div class="col-lg-3 col-md-3 col-sm-3<?php if($aux % 4 == 0){echo 'border-right-none'; } ?>">
        <figure>    
            <a href="<?php the_permalink(); ?>">                                
                <?php echo $thumbnail; ?>                                
            </a>
        </figure>      
        <div class="info">
             <a href="<?php the_permalink(); ?>">
                <h4>
                    <?php
                        the_title();
                    ?>                                        
                </h4>
            </a>
            <p> <?php echo $product->get_price_html(); echo "<br>"; echo $product->is_in_stock() ? 'Em estoque' : 'Fora do estoque'; ?> </p>                                
                <?php
                    global $product;

                    echo apply_filters( 'woocommerce_loop_add_to_cart_link',
                        sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s">%s</a>',
                            esc_url( $product->add_to_cart_url() ),
                            esc_attr( isset( $quantity ) ? $quantity : 1 ),
                            esc_attr( $product->id ),
                            esc_attr( $product->get_sku() ),
                            esc_attr( isset( $class ) ? $class : 'btn-lg btn-block hvr-wobble-horizontal' ),
                            esc_html( $product->add_to_cart_text() )
                        ),
                    $product );
                ?>
        </div>
    </div>  
<?php wp_reset_postdata(); ?>
